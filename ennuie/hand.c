#include <stdio.h>

void handdown() {
  printf("                  \n");
  printf("      °°°°°°      \n");
  printf("     ° #  # °     \n");
  printf("     °  ^   °     \n");
  printf("      °___ °      \n");
  printf("       °°°°       \n");
  printf("        ||        \n");
  printf("       /||\\      \n");
  printf("      / || \\     \n");
  printf("     /  ||  \\    \n");
  printf("        ||        \n");
  printf("        ||        \n");
  printf("        /\\       \n");
  printf("       /  \\      \n");
  printf("      /    \\     \n");
}

void handup() {
  printf("                  \n");
  printf("      °°°°°°      \n");
  printf("     ° #  # °     \n");
  printf("     °  ^   °     \n");
  printf("      °___ °      \n");
  printf("       °°°°       \n");
  printf("        ||        \n");
  printf("  ------||------  \n");
  printf("        ||        \n");
  printf("        ||        \n");
  printf("        ||        \n");
  printf("        ||        \n");
  printf("        /\\       \n");
  printf("       /  \\      \n");
  printf("      /    \\     \n");
}

int main() {

  int i = 5;
  system("clear");
  while (i--)
  {
    system("clear");
    handdown();
    fflush(stdout);
    sleep(1);
    system("clear");
    handup();
    fflush(stdout);
    sleep(1);
  }
  return 0;
}
