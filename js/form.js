function desactivatetip() {
    var tip = document.querySelectorAll('.tip'),
        tiplenght = tip.length;

    for(var i = 0; i < tiplenght; i++) {
        tip[i].style.display = 'none';
    }
};

function gettip(elements) {
    while(elements = elements.nexSibling) {
        if(elements.className === 'tip') {
            return elements;
        }
    }
    return false;
};

var check = {};

check['sexe'] = function () {
    var sexe = document.getElementsByName('sexe'),
    tipstyle = gettip(sexe[1].parentNode).style;

    if(sexe[0].check || sexe[1].check) {
        tipstyle.display = 'none';
        return true;
    }
    else {
        tipstyle.display = 'inline-block';
        return false;
    }
};

check['lastname'] = function() {
    var lastname = document.getElementById('lastname'),
    tipstyle = gettip(lastname).style,
    lastnamelenght = lastname.value.length;


    if (lastname >= 2) {
        name.className = 'correct';
        tipstyle.display = 'none';
        return true;
    }
    else {
        name.className = 'incorrect';
        tipstyle.display = 'inline-block';
        return false
    }
};

check['prénom'] = check['lastname'];

check['age'] = function () {
    var age = document.getElementById('age'),
    tipstyle = gettip(age).style,
    valeurage = isNaN(age.value);
    

    if (isNaN(valeurage) && age >= 5 && age <= 140) {
        name.className = 'correct';
        tipstyle.display = 'none';
        return true;
    }
    else {
        name.className = 'incorrect';
        tipstyle = 'inline-block';
        return true;
    }
};

check['pseudo'] = function () {
    var pseudo = document.getElementById('pseudo'),
    tipstyle = gettip(pseudo).style,
    pseudolenght = pseudo.value.length;

    if (pseudolenght >= 4) {
        name.className = 'correct';
        tipstyle.display = 'none';
        return true;
    }
    else {
        name.className = 'incorrect';
        tipstyle.display = 'inline-block';
        return false;
    }
    
};

check['passwd','passwd2'] = function () {
    var passwd = document.getElementById('passwd'),
    passwd2 = document.getElementById('passwd2'),
    tipstyle = gettip(passwd).style,
    tipstyleconfirm = gettip(passwd2).style,
    passwdlenght = passwd.value.length
    passwd2lenght = passwd2.value.length;
    
    for (var i =  0; i < passwdlenght && i < passwd2lenght; i++){
    if (passwdlenght >= 6  && passwd2lenght >= 6  || passwdlenght[i] === passwd2lenght[i]) {
        name.className = 'correct';
        tipstyle.display = 'none';
        tipstyleconfirm.display = 'none';
        return true;
    }
    else {
        name.className = 'incorrect';
        tipstyle.display = 'inline-block';
        tipstyleconfirm.display = 'inline-block';
        return false;
        break;
    }
}
};

check['pays'] = function () {
    var pays = document.getElementById('pays'),
    tipstyle = gettip(pays).style;

    if (pays.options[pays.selectedIndex].value != 'none') {
        tipstyle.display = 'none'
        return true;
    }
    else if (pays.options[pays.selectedIndex].value = pays[0]){
        name.className = ' incorrect';
        tipstyle.display = 'inline-block';
        return false;
    }
    else {
        tipstyle.display = 'inline-block';
        return false;
    }
};

(function() {
    var monform = document.getElementById('monform'),
    inputs = document.querySelectorAll('input[type=text], input[type=password]'),
    inputslenght = inputs.length

    for (var i = 0; i < inputslenght; i++) {
        inputs[i].addEventListener('keyup', function(e){
            check[e.target.id](e.target.id);
        })
    }
});

monform.addEventListener('submit', function(e) {
    var result = true;

    for (var i in check) {
        result = check[i](i) && result;
    }
    e.preventDefault();
});

monform.addEventListener('reset', function() {
    for (var i = 0; i < inputslenght; i++) {
        input[i].className= '';
    }
    desactivatetip();
});


desactivatetip();
