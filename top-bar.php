<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://kit.fontawesome.com/b4a1a3f013.js" crossorigin="anonymous"></script>

<div id="top-bar">
    <span id="top-bar-content">
      NOUS CONTACTER : <a href="tel:0777256819">07 77 25 68 19</a> /
        <a href="mailto:e.pannequin.formation@gmail.com">e.pannequin.formation@gmail.com</a>
    </span>

    <span id="top-bar-social">
      <ul>
        <li class="twitter">
          <a href="https://twitter.com/EPFormation17" target="_blank">
            <i class="fab fa-twitter" style="font-size:19px"></i>
          </a>
        </li>
        <li class="facebook">
          <a href="https://www.facebook.com/erick.pannequinpro"  target="_blank">
           <i class="fab fa-facebook" style="font-size:19px"></i>
        </li>
        <li class="linkedin">
          <a href="https://www.linkedin.com/in/erick-pannequin-epformation/" target="_blank">
          <i class="fab fa-linkedin" style="font-size:19px"></i>
          </a>
        </li>
      </ul>
    </span>
</div>
