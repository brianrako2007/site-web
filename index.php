<!DOCTYPE html>
<html lang="fr">
<head>
	<title>EPFormation</title>
	<script src="https://kit.fontawesome.com/b4a1a3f013.js" crossorigin="anonymous"></script>
	<script src="https://apps.elfsight.com/p/platform.js" defer></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/style.css"/>
	<link rel="icon" type="image/png" href="images/logo.png"/>
	<meta charset="UTF-8">
</head>
<body>
	<div id="fb-root"></div>
	<script async defer crossorigin="anonymous" src="https/connect.facebook.net/en_US/sdk.js#xfbml=1&version=v7.0"></script>

	<div id="bloc-page">

	<header>
	<?php include 'top-bar.php'; ?>
	<?php include 'header.php'; ?>
	</header>

	<iframe class="camion" width="1786" height="718" src="https://www.youtube.com/embed/ysLf5j3Kac8" frameborder="0"
	allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


	<div class="bloc_center">
	<h2 class="title_box">NOTRE ORGANISME DE FORMATION</h2>
	<div class="text-form">
	<p>La prévention est l’affaire de tous quelque soit la nature de votre activité, la taille<br>
	  de votre entreprise et l’organisation de votre établissement. EP-Formation vous<br>
	   propose un large choix de formations décomposé en 5 familles.</p>
	</div>
	<h2 class="certif">ORGANISME CERTIFIÉ<h2>
	<img class="l_inrs" src="images/logo-inrs.jpg" alt="inrs">
	<img class="l_datadoc"src="images/logo_datadoc.jpg" alt="datadoc">

	<div class="feed_fb">
	<div class="elfsight-app-241b8751-e564-457e-9bc8-5e509e213218"></div>
	</div>

	</div>

	<div id="bloc-formation">

	<h2 class="title_formation">NOS FORMATIONS</h2>

	<div id="list-formation">

	    <div class="incendie">
	      <figure>
	        <a href="incendie.php">
	          <img  src="images/logo_incendie.png" alt="" class="l-incendie">
	        </a>
	      </figure>
	      <a class="txt-form" href="incendie.php">INCENDIE</a>
	    </div>

	    <div class="secourisme">
	      <figure>
	        <a href="secourisme.php">
	          <img src="images/logo-secourisme.png" alt="" class="l-secourisme">
	        </a>
	      </figure>
	      <a class="txt-form" href="secourisme.php">SECOURISME</a>
	    </div>

	  <div class="prev_risque">
	        <figure>
	          <a href="pre-risques.php">
	            <img src="images/logo-prev.png" alt="" class="l-prev">
	          </a>
	        </figure>
	        <a class="txt-form" href="pre-risques.php" class="text-form">PREVENTION DES RISQUES</a>
	      </div>

	  <div class="evac">
	      <figure>
	        <a href="evacuation.php">
	          <img src="images/logo-evac.png" alt="" class="l-evac"></a>
	      </figure>
	      <a class="txt-form" href="evacuation.php">EVACUATION</a>
	    </div>

	  <div class="elec">
	      <figure>
	        <a href="elec.php">
	          <img src="images/logo-elec.png" alt="" class="l-elec">
	        </a>
	      </figure>
	      <a href="elec.php" class="txt-form">HABILITATION ELECTRIQUE</a>
	  </div>

	  <div class="unit-mobile">
	      <figure>
	        <a href="unit-mobile.php">
	          <img src="images/logo_incendie.png" alt="" class="l-unit">
	        </a>
	      </figure>
	      <a href="unit-mobile.php" class="txt-form">UNITE MOBILE</a>
	  </div>
	</div>
	<div id="divider">
	<span class="divider-separator"></span>
	</div>
	<h2 class="t-slide"><span class="first_part">Ils nous ont fait confiance,</span>   pourquoi pas vous ?</h2>

	<?php include 'carousel.php'; ?>

	<footer>
	<?php include 'footer.php'; ?>
	</footer>

	</div>

</body>
</html>
