

<div id="footer">

<div id="incendie">
    <div class="img_title">
    <figure>
      <a href="incendie.php">
        <img src="images/logo_incendie.png" alt="" class="f-incendie">
    </figure>
    <a href="incendie.php" class="t-incendie">Incendie</a>
  </div>
    <div class="txt">
      <ul>
        <li><a href="#">Initiation à la sécurité incendie</a></li>
        <li><a href="#">Manipulation des extincteurs</a></li>
        <li><a href="#">Équipier de première intervention</a></li>
        <li><a href="#">Équipier de seconde intervention</a></li>
        <li><a href="#">Initiation au système de sécurité incendie</a></li>
        <li><a href="#">Perfectionnement au système de sécurité<br> incendie</a></li>
      </ul>
    </div>
  </div>

<div id="secourisme">
  <div class="img_title">
    <figure>
    <a href="secourisme.php">
        <img src="images/logo-secourisme.png" alt="" class="f-secour">
      </a>
    </figure>
  <a href="secourisme.php" class="t-secour">Secourisme</a>
</div>
    <div class="txt">
      <ul>
        <li><a href="#">Initiation au défibrillateur</a></li>
        <li><a href="#">Initiation aux gestes de premiers secours</a></li>
        <li><a href="#">Sauveteur secouriste du travail</a></li>
        <li><a href="#">Maintien et actualisation des compétences SST</a></li>
      </ul>
    </div>
</div>

<div id="prev">
  <div class="img_title">
  <figure>
    <a href="pre-risques.php">
      <img src="images/logo-prev.png" alt=""class="f-prev">
    </a>
  </figure>
  <a href="pre-risques.php" class="t-prev">Prévention des risques professionnels</a>
</div>
  <div class="txt">
  <ul>
    <li><a href="#">Gestuelle et posture en milieu de travail</a></li>
    <li><a href="#">Gestuelle et posture en milieu bureautique</a></li>
    <li><a href="#">Gestuelle et posture en milieu de santé</a></li>
  </ul>
  </div>
  </div>

  <div id="evac">
    <div class="img_title">
    <figure>
      <a href="evacuation.php">
        <img src="images/logo-evac.png" alt="" class="f-evacuation">
      </a>
    </figure>
    <a href="evacuation.php" class="t-evac">Évacuation</a>
  </div>
    <div class="txt">
      <ul>
        <li><a href="#">Exercice d’évacuation</a></li>
        <li><a href="#">Organisation de l’évacuation</a></li>
        <li><a href="#">Mise en sécurité de patient</a></li>
      </ul>
    </div>
  </div>

  <div id="elec">
  <div class="img_title">
    <figure>
      <a href="elec.php">
        <img src="images/logo-elec.png" alt="" class="f-evacuation">
      </a>
    </figure>
    <a href="elec.php" class="t-elec">Habilitation électrique</a>
  </div>
    <div class="txt">
      <ul>
        <li><a href="#">H0/B0 – H0(V)</a></li>
        <li><a href="#">BS Chargé d’intervention</a></li>
        <li><a href="#">BE Manœuvre</a></li>
        <li><a href="#">BS BE Manœuvre</a></li>
      </ul>
    </div>
  </div>

</div>

<div id="bottom-footer">
<div class="copyright">
<p>Copyright © 2020 ep-formation.pro - Tous droits réservé</p>
</div>

<div class="navigation"><a href="#">
Politique de confidentialité
</a></div>
