
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <link rel="stylesheet" href="assets/css/animate.css">

        <link rel="stylesheet" href="assets/css/media-queries.css">
        <link rel="stylesheet" href="assets/css/carousel.css">

                <script src="assets/js/jquery-3.3.1.min.js"></script>
                		<script src="assets/js/jquery-migrate-3.0.0.min.js"></script>
                		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
                		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
                        <script src="assets/js/jquery.backstretch.min.js"></script>
                        <script src="assets/js/wow.min.js"></script>
                        <script src="assets/js/scripts.js"></script>

<div class="top-content">
        	<div class="container-fluid">
        		<div id="carousel-example" class="carousel slide" data-ride="carousel">
        			<div class="carousel-inner row w-100 mx-auto" role="listbox">
            			<div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 active">
							<img src="carousel/agrisem.png" class="img-fluid mx-auto d-block" alt="img1">
						</div>
						<div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
							<img src="carousel/athenee.png" class="img-fluid mx-auto d-block" alt="img2">
						</div>
						<div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
							<img src="carousel/camus.png" class="img-fluid mx-auto d-block" alt="img3">
						</div>
						<div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
							<img src="carousel/champignon_renaud.png" class="img-fluid mx-auto d-block" alt="img4">
						</div>
						<div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
							<img src="carousel/coetc.png" class="img-fluid mx-auto d-block" alt="img5">
						</div>
						<div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
							<img src="carousel/courir.png" class="img-fluid mx-auto d-block" alt="img6">
						</div>
						<div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
							<img src="carousel/customdev.png" class="img-fluid mx-auto d-block" alt="img7">
						</div>
						<div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
							<img src="carousel/durand.png" class="img-fluid mx-auto d-block" alt="img8">
						</div>
            <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
							<img src="carousel/lafont.png" class="img-fluid mx-auto d-block" alt="img9">
						</div>
            <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
							<img src="carousel/langely.png" class="img-fluid mx-auto d-block" alt="img10">
						</div>
            <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
							<img src="carousel/macdo.png" class="img-fluid mx-auto d-block" alt="img11">
						</div>
            <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
							<img src="carousel/master.png" class="img-fluid mx-auto d-block" alt="img12">
						</div>
            <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
							<img src="carousel/smash.png" class="img-fluid mx-auto d-block" alt="img13">
						</div>
            <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
							<img src="carousel/coup_eco.png" class="img-fluid mx-auto d-block" alt="img14">
						</div>
						<div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
							<img src="carousel/vallen.png" class="img-fluid mx-auto d-block" alt="img15">
						</div>
        			</div>
        			<a class="carousel-control-prev" href="#carousel-example" role="button" data-slide="prev">
								<span class="fas fa-angle-left" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carousel-example" role="button" data-slide="next">
						<span class="fas fa-angle-right" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>

        		</div>
        	</div>
        </div>
